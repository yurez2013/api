<?php

error_reporting(-1);

require_once 'lib/meekrodb.2.3.class.php';
require_once 'config/config.php';
require_once 'product/Users.php';
require_once 'product/Product.php';
require_once 'product/Category.php';

$user = new Users();

if(isset($_GET['Category'])) {
    $category = new Category();
    $category->index($_GET);
}

if(isset($_GET['Product'])) {
    $product = new Product();
    $product->index($_GET);
}
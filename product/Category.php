<?php

class Category
{

    /**
     * @param $urlGet - параметри для методів
     */
    public function index($urlGet)
    {
        if(isset($_GET['username']) && isset($_GET['password'])) {
            $user = new Users();
            $user = $user->auth($_GET['username'], $_GET['password']);
        }

        $url = $urlGet['Category'];

        switch ($url) {
            case 'getCategory':
                $this->getCategory();
                break;
            case 'addCategory':
                if ($user) $this->addCategory($urlGet['name']);
                break;
            case 'editCategory':
                if ($user) $this->editCategory($urlGet['id'], $urlGet['name']);
                break;
            case 'deleteCategory':
                if ($user) $this->deleteCategory($urlGet['id']);
                break;
        }

    }

    /**
     * @return array|string
     */
    protected function getCategory()
    {
        $category = DB::query('SELECT * FROM category');
        echo is_array($category) ? json_encode($category) : [];
    }

    /**
     * @param $category_name
     * @return mixed
     */
    protected function addCategory($category_name)
    {
        if (isset($category_name) && !empty($category_name)) {
            return DB::insert('category', array(
                'category_name' => $category_name,
            ));
        }
    }

    /**
     * @param $id
     * @param $category_name
     * @return mixed
     */
    protected function editCategory($id, $category_name)
    {
        if (isset($category_name) && !empty($category_name) && isset($id) && !empty($id)) {
            return DB::update('category', array(
                'category_name' => $category_name
            ), "id=%s", $id);
        }
    }

    /**
     * @param $id
     */
    protected function deleteCategory($id)
    {
        DB::delete('category', "id=%s", $id);
    }

}
<?php

class Product {

    /**
     * @param $urlGet - параметри для методів
     */
    public function index($urlGet)
    {
        if(isset($_GET['username']) && isset($_GET['password'])) {
            $user = new Users();
            $user = $user->auth($_GET['username'], $_GET['password']);
        }

        $url = $urlGet['Product'];

        switch ($url) {
            case 'getProduct': $this->getProduct($urlGet['category']);
            break;
            case 'addProduct': if ($user) $this->addProduct($urlGet['name'], $urlGet['category']);
            break;
            case 'editProduct':  if ($user) $this->editProduct($urlGet['id'], $urlGet['name'], $urlGet['category']);
            break;
            case 'deleteProduct':  if ($user) $this->deleteProduct($urlGet['id']);
            break;
        }
    }

    /**
     * @param $categoryId
     * @return array|string
     */
    protected function getProduct($categoryId)
    {
        $product = DB::query('SELECT * FROM products WHERE category = %d', $categoryId);
        echo is_array($product) ? json_encode($product) : [];
    }

    /**
     * @param $product
     * @param $category
     * @return mixed
     */
    protected function addProduct($product, $category)
    {
        if(isset($product) && !empty($product) && isset($category) && !empty($category)) {
            return DB::insert('products', array(
                'name' => $product,
                'category' => $category
            ));
        }
    }

    /**
     * @param $id
     * @param $product
     * @param $category
     */
    protected function editProduct($id, $product, $category)
    {
        DB::update('products', array(
            'name' => $product,
            'category' => $category
        ), "id=%s", $id);
    }

    /**
     * @param $id
     */
    protected function deleteProduct($id)
    {
        DB::delete('products', "id=%s", $id);
    }

}
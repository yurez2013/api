<?php

class Users {

    /**
     * @param $urlGet - параметри для методів
     */
    public function index($urlGet)
    {
        $url = $urlGet['User'];

        switch ($url) {
            case 'register': $this->register($urlGet['username'], $urlGet['password']);
            break;
            case 'auth': $this->auth($urlGet['username'], $urlGet['password']);
            break;
        }

    }

    /**
     * @param $username
     * @param $password
     * @return bool|mixed
     */

    public function register($username, $password)
    {
        $hashPass = md5($password);

        $users = DB::queryFirstColumn('SELECT `login` FROM users');

        if(!in_array($username, $users)) {

            return DB::insert('users', array(
                'login' => $username,
                'password' => $hashPass
            ));

        }

        return false;

    }

    /**
     * @param $username
     * @param $password
     * @return bool - повертає true якщо авторизований
     */
    public function auth($username, $password)
    {
        $hashPass = md5($password);
        $user = DB::query('SELECT * FROM users WHERE login = %s AND password = %s', $username, $hashPass);

        return isset($user[0]['login']) && !empty($user[0]['login']) ? true : false;
    }

}
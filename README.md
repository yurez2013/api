### Production

1. Потрібно розгорнути базу даних, яка знаходиться в папці sql/prod.sql
2. Налаштувати зэднання з базою даних config/config.php
3. Робота з Api

        Auth - це клас юзерів
            auth - метод авторизації юзера
            register - регістрації
            username - імя юзера
            password - пароль юзера
      
        Product - це клас продуктів
        
            getProduct - метод отримання продуктів по категоріях
            addProduct - метод додання продуктів
            editProduct - метод редагування продуктів
            deleteProduct - метод видалення продуктів
            
            name - імя продукту
            category - категорія продукту
            id - id продукту
            
        Category - це клас категорій
        
            addCategory - метод додання категорій
            addCategory - метод додання категорій
            editCategory - метод редагування категорій
            deleteCategory - метод видалення категорій
            name - імя категорії
            category - категорія продукту
            id - id категорії

    # Для отримання продуктів
        base_url/?Product=getProduct&category=1
    # Для отримання категорій
            base_url/?Category=getCategory
    
    # Для регістрації
     base_url/?Auth=register&username=y.ludchak&password=12345
    
    # Для авторизації
     base_url/?Auth=auth&username=y.ludchak&password=12345
    
    # Для добавлення продуктів для авторизованих юзерів
        base_url/?Auth=auth&username=y.ludchak&password=12345&Product=addProduct&name=xiomi&category=2
    # Для добавлення категорій для авторизованих юзерів
        base_url/?Auth=auth&username=y.ludchak&password=12345&Category=addCategory&name=телефоны
    
    # Для редагування продуктів для авторизованих юзерів
        base_url/?Auth=auth&username=y.ludchak&password=12345&Product=editProduct&id=1&name=xiomi&category=2
        
    # Для редагування категорій для авторизованих юзерів
        base_url/?Auth=auth&username=y.ludchak&password=12345&Category=editCategory&id=1&name=sd

    # Для видалення продуктів для авторизованих юзерів
        base_url/?Auth=auth&username=y.ludchak&password=12345&Product=deleteProduct&id=1
   
    # Для видалення категорій для авторизованих юзерів
        base_url/?Auth=auth&username=y.ludchak&password=12345&Category=deleteCategory&id=1